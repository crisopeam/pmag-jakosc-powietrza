package com.agkw.jakoscpowietrza;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Marker;
import android.support.v4.content.ContextCompat;
import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import android.location.Location;
import android.location.LocationManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TextView mTextView;
    private RequestQueue queue;
    private JSONObject dane_pomiarowe;
    private boolean LocPermissionsGranted = false;
    private FusedLocationProviderClient  LocProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if(isServicesOK()) getLocationPermission();

    }

    private void getLocationPermission(){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                LocPermissionsGranted = true;

            }else{
                ActivityCompat.requestPermissions(this, permissions,1234);
            }
        }else{
            ActivityCompat.requestPermissions(this, permissions, 1234);
        }
        Log.d("getLoc","getLocationPermission");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        LocPermissionsGranted = false;

        switch(requestCode){
            case 1234:{
                if(grantResults.length > 0){
                    for(int i = 0; i < grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            LocPermissionsGranted = false;
                            return;
                        }
                    }
                    LocPermissionsGranted = true;
                }
            }
        }
        Log.d("onReq","onRequestPermissionsResult");
    }

    private void getDeviceLocation(){

        LocProviderClient = LocationServices.getFusedLocationProviderClient(this);
        Log.d("getDev","getDeviceLocation1");
        try{
            if(LocPermissionsGranted){

                final Task location =  LocProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            Location currentLocation = (Location) task.getResult();
                            LatLng loc = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc,10));

                            Toast.makeText(MapsActivity.this,"Znaleziono lokalizację", Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(MapsActivity.this, "Nie można uzyskać bieżącej lokalizacji", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Log.e("getDev", "getDeviceLocation: SecurityException: " + e.getMessage() );
        }
    }

    public boolean isServicesOK(){
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            Toast.makeText(MapsActivity.this, "Lokalizacja urządzenia wyłączona", Toast.LENGTH_SHORT).show();
            return false;

        }
        else return true;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        queue = Volley.newRequestQueue(this);

        if (LocPermissionsGranted) {
            getDeviceLocation();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);

        } else {
            LatLng pw = new LatLng(52.220343, 21.011022);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pw, 10));
        }

        String url = "http://api.gios.gov.pl/pjp-api/rest/station/findAll";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject stacja = response.getJSONObject(i);
                        JSONObject dane = new JSONObject();
                        String id = stacja.getString("id");
                        String nazwa = stacja.getString("stationName");
                        double lat = stacja.getDouble("gegrLat");
                        double lon = stacja.getDouble("gegrLon");
                        dane.put("nazwa", nazwa);
                        dane.put("lat", lat);
                        dane.put("lon", lon);
                        String url = String.format("http://api.gios.gov.pl/pjp-api/rest/station/sensors/%s", id);
                        requestSensors(url, dane, id);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText("That didn't work!");
            }
        }
        );
        queue.add(jsonArrayRequest);

    }

    private void requestSensors(String url, final JSONObject dane, final String id) {
        JsonArrayRequest sensorRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                boolean czyJestPM = false;

                try {
                    MarkerOptions mMarker = new MarkerOptions();
                    mMarker.title(dane.getString("nazwa"));
                    mMarker.snippet("");
                    mMarker.visible(false);
                    mMarker.position(new LatLng(dane.getDouble("lat"), dane.getDouble("lon")));
                    mMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    Marker pokaz_marker=mMap.addMarker(mMarker);
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject pylek = response.getJSONObject(i);
                        JSONObject param = pylek.getJSONObject("param");
                        String paramCode = param.getString("paramCode");
                        if (paramCode.equals("PM10")){
                            Log.d("mapsActivity", "pylek10");
                            String pm10_id = pylek.getString("id");
                            String url = String.format("http://api.gios.gov.pl/pjp-api/rest/data/getData/%s", pm10_id);
                            requestPM(url, dane, pokaz_marker);
                            czyJestPM = true;
                        }
                        if (paramCode.equals("PM2.5")) {
                            String pm25_id = pylek.getString("id");
                            String url = String.format("http://api.gios.gov.pl/pjp-api/rest/data/getData/%s", pm25_id);
                            requestPM(url, dane, pokaz_marker);
                            czyJestPM = true;
                        }
                    }

                    if (!czyJestPM) {
                        Log.d("mapsActivity", "Usuwam marker");
                        pokaz_marker.remove();
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText("That didn't work!");
            }
        });
        queue.add(sensorRequest);
    }
    private void requestPM(String url, final JSONObject dane, final Marker pokaz_marker) {
        JsonObjectRequest pmRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray wartosci = response.getJSONArray("values");
                    JSONObject najnowsze = wartosci.getJSONObject(wartosci.length() - 1);
                    double wartosc = najnowsze.getDouble("value");
                    String klucz = response.getString("key");
                    dane.put(klucz, wartosc);
                    pokaz_marker.setSnippet(pokaz_marker.getSnippet() + "Wartość " + klucz + " = " + wartosc + ".  ");
                    double pm10 = dane.optDouble("PM10");
                    double pm25 = dane.optDouble("PM2.5");
                    float color;
                    if (Double.isNaN(pm10) || pm10 <= 60) {
                        if (Double.isNaN(pm25) || pm25 <= 37)
                            color = BitmapDescriptorFactory.HUE_GREEN;
                        else if (pm25 <= 85)
                            color = BitmapDescriptorFactory.HUE_ORANGE;
                        else
                            color = BitmapDescriptorFactory.HUE_RED;
                    }
                    else if (pm10 <= 141) {
                        if (Double.isNaN(pm25) || pm25 <= 85)
                            color = BitmapDescriptorFactory.HUE_ORANGE;
                        else
                            color = BitmapDescriptorFactory.HUE_RED;
                    }
                    else
                        color = BitmapDescriptorFactory.HUE_RED;

                    pokaz_marker.setIcon(BitmapDescriptorFactory.defaultMarker(color));
                    pokaz_marker.setVisible(true);

                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText("That didn't work!");
            }
        });
        queue.add(pmRequest);
    }
}
